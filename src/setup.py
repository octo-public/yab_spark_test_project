from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
extensions = [
    Extension("cython_demo.reac_demo",  ["reac_demo.py"]), Extension("cython_demo.product", ["product.py"])
]
ext_modules = cythonize(
    extensions,
    compiler_directives={'language_level' : "3"}
)
setup(
    name='cython_demo',
    cmdclass={'build_ext': build_ext},
    ext_modules=ext_modules
)
