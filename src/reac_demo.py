# Chargement des villes
import math
import random
import time

from pyspark import SparkContext, SQLContext

from product import Product

from pyspark.sql.session import SparkSession

villes = ["Paris","Marseille","Lyon","Toulouse","Nice","Nantes","Strasbourg","Montpellier","Bordeaux","Lille","Rennes","Reims","Le Havre","Saint-Étienne","Toulon","Grenoble","Dijon","Angers","Nîmes","Villeurbanne","Saint-Denis","Le Mans","Clermont-Ferrand","Aix-en-Provence","Brest","Limoges","Tours","Amiens","Perpignan","Metz","Boulogne-Billancourt","Besançon","Orléans","Rouen","Mulhouse","Caen","Saint-Denis","Nancy","Argenteuil","Saint-Paul","Montreuil","Roubaix","Tourcoing","Dunkerque","Nanterre","Créteil","Avignon","Vitry-sur-Seine","Poitiers","Courbevoie","Fort-de-France","Versailles","Colombes","Asnières-sur-Seine","Aulnay-sous-Bois","Saint-Pierre","Rueil-Malmaison","Pau","Aubervilliers","Champigny-sur-Marne","Le Tampon","Antibes","Saint-Maur-des-Fossés","La Rochelle","Cannes","Béziers","Calais","Saint-Nazaire","Colmar","Drancy","Bourges","Mérignac","Ajaccio","Issy-les-Moulineaux","Levallois-Perret","La Seyne-sur-Mer","Quimper","Noisy-le-Grand","Valence","Villeneuve-d'Ascq","Neuilly-sur-Seine","Antony","Vénissieux","Cergy","Troyes","Clichy","Pessac","Les Abymes","Ivry-sur-Seine","Chambéry","Lorient","Niort","Sarcelles","Montauban","Villejuif","Saint-Quentin","Hyères","Cayenne","Épinay-sur-Seine","Saint-André","Beauvais","Maisons-Alfort","Cholet","Meaux","Chelles","Pantin","Fontenay-sous-Bois","La Roche-sur-Yon","Bondy","Vannes","Saint-Louis","Fréjus","Arles","Clamart","Évry","Le Blanc-Mesnil","Narbonne","Sartrouville","Grasse","Annecy","Laval","Belfort","Vincennes","Charleville-Mézières","Évreux","Sevran","Albi","Montrouge","Bobigny","Martigues","Saint-Ouen","Brive-la-Gaillarde","Suresnes","Carcassonne","Cagnes-sur-Mer","Corbeil-Essonnes","Saint-Brieuc","Blois","Bayonne","Aubagne","Châlons-en-Champagne","Meudon","Châteauroux","Saint-Malo","Chalon-sur-Saône","Sète","Puteaux","Alfortville","Salon-de-Provence","Massy","Mantes-la-Jolie","Bastia","Vaulx-en-Velin","Saint-Herblain","Le Cannet","Valenciennes","Istres","Gennevilliers","Boulogne-sur-Mer","Livry-Gargan","Saint-Priest","Rosny-sous-Bois","Caluire-et-Cuire","Angoulême","Douai","Tarbes","Wattrelos","Castres","Choisy-le-Roi","Talence","Thionville","Arras","Alès","Garges-lès-Gonesse","Gap","Saint-Laurent-du-Maroni","Melun","Bourg-en-Bresse","Noisy-le-Sec","Compiègne","La Courneuve","Le Lamentin","Marcq-en-Barœul","Saint-Germain-en-Laye","Rezé","Bron","Anglet","Gagny","Chartres","Bagneux","Saint-Martin-d'Hères","Montluçon","Pontault-Combault","Poissy","Draguignan","Joué-lès-Tours","Savigny-sur-Orge","Cherbourg-Octeville","Saint-Joseph","Le Port","Colomiers","Saint-Martin","Villefranche-sur-Saône","Stains","Saint-Benoît","Échirolles","Villepinte","Roanne","Montélimar","Saint-Chamond","Nevers","Conflans-Sainte-Honorine","Auxerre","Sainte-Geneviève-des-Bois","Châtillon","Bagnolet","Vitrolles","Thonon-les-Bains","Neuilly-sur-Marne","Haguenau","Marignane","Saint-Raphaël","Tremblay-en-France","La Ciotat","Six-Fours-les-Plages","Creil","Agen","Romans-sur-Isère","Montigny-le-Bretonneux","Le Perreux-sur-Marne","Franconville","Annemasse","Villeneuve-Saint-Georges","Saint-Leu","Mâcon","Cambrai","Lens","Houilles","Épinal","Châtenay-Malabry","Schiltigheim","Sainte-Marie","Liévin","Châtellerault","Meyzieu","Goussainville","Viry-Châtillon","Dreux","L'Haÿ-les-Roses","Plaisir","Mont-de-Marsan","Maubeuge","Nogent-sur-Marne","Les Mureaux","Clichy-sous-Bois","La Possession","Dieppe","Chatou","Vandœuvre-lès-Nancy","Malakoff","Palaiseau","Pontoise","Charenton-le-Pont","Rillieux-la-Pape"]
# Chargement des regions
regions = ["Île-de-France","Provence-Alpes-Côte d'Azur","Rhône-Alpes","Midi-Pyrénées","Provence-Alpes-Côte d'Azur","Pays de la Loire","Alsace","Languedoc-Roussillon","Aquitaine","Nord-Pas-de-Calais","Bretagne","Champagne-Ardenne","Haute-Normandie","Rhône-Alpes","Provence-Alpes-Côte d'Azur","Rhône-Alpes","Bourgogne","Pays de la Loire","Languedoc-Roussillon","Rhône-Alpes","La Réunion","Pays de la Loire","Auvergne","Provence-Alpes-Côte d'Azur","Bretagne","Limousin","Centre-Val de Loire","Picardie","Languedoc-Roussillon","Lorraine","Île-de-France","Franche-Comté","Centre-Val de Loire","Haute-Normandie","Alsace","Basse-Normandie","Île-de-France","Lorraine","Île-de-France","La Réunion","Île-de-France","Nord-Pas-de-Calais","Nord-Pas-de-Calais","Nord-Pas-de-Calais","Île-de-France","Île-de-France","Provence-Alpes-Côte d'Azur","Île-de-France","Poitou-Charentes","Île-de-France","Martinique","Île-de-France","Île-de-France","Île-de-France","Île-de-France","La Réunion","Île-de-France","Aquitaine","Île-de-France","Île-de-France","La Réunion","Provence-Alpes-Côte d'Azur","Île-de-France","Poitou-Charentes","Provence-Alpes-Côte d'Azur","Languedoc-Roussillon","Nord-Pas-de-Calais","Pays de la Loire","Alsace","Île-de-France","Centre-Val de Loire","Aquitaine","Corse","Île-de-France","Île-de-France","Provence-Alpes-Côte d'Azur","Bretagne","Île-de-France","Rhône-Alpes","Nord-Pas-de-Calais","Île-de-France","Île-de-France","Rhône-Alpes","Île-de-France","Champagne-Ardenne","Île-de-France","Aquitaine","Guadeloupe","Île-de-France","Rhône-Alpes","Bretagne","Poitou-Charentes","Île-de-France","Midi-Pyrénées","Île-de-France","Picardie","Provence-Alpes-Côte d'Azur","Guyane","Île-de-France","La Réunion","Picardie","Île-de-France","Pays de la Loire","Île-de-France","Île-de-France","Île-de-France","Île-de-France","Pays de la Loire","Île-de-France","Bretagne","La Réunion","Provence-Alpes-Côte d'Azur","Provence-Alpes-Côte d'Azur","Île-de-France","Île-de-France","Île-de-France","Languedoc-Roussillon","Île-de-France","Provence-Alpes-Côte d'Azur","Rhône-Alpes","Pays de la Loire","Franche-Comté","Île-de-France","Champagne-Ardenne","Haute-Normandie","Île-de-France","Midi-Pyrénées","Île-de-France","Île-de-France","Provence-Alpes-Côte d'Azur","Île-de-France","Limousin","Île-de-France","Languedoc-Roussillon","Provence-Alpes-Côte d'Azur","Île-de-France","Bretagne","Centre-Val de Loire","Aquitaine","Provence-Alpes-Côte d'Azur","Champagne-Ardenne","Île-de-France","Centre-Val de Loire","Bretagne","Bourgogne","Languedoc-Roussillon","Île-de-France","Île-de-France","Provence-Alpes-Côte d'Azur","Île-de-France","Île-de-France","Corse","Rhône-Alpes","Pays de la Loire","Provence-Alpes-Côte d'Azur","Nord-Pas-de-Calais","Provence-Alpes-Côte d'Azur","Île-de-France","Nord-Pas-de-Calais","Île-de-France","Rhône-Alpes","Île-de-France","Rhône-Alpes","Poitou-Charentes","Nord-Pas-de-Calais","Midi-Pyrénées","Nord-Pas-de-Calais","Midi-Pyrénées","Île-de-France","Aquitaine","Lorraine","Nord-Pas-de-Calais","Languedoc-Roussillon","Île-de-France","Provence-Alpes-Côte d'Azur","Guyane","Île-de-France","Rhône-Alpes","Île-de-France","Picardie","Île-de-France","Martinique","Nord-Pas-de-Calais","Île-de-France","Pays de la Loire","Rhône-Alpes","Aquitaine","Île-de-France","Centre-Val de Loire","Île-de-France","Rhône-Alpes","Auvergne","Île-de-France","Île-de-France","Provence-Alpes-Côte d'Azur","Centre-Val de Loire","Île-de-France","Basse-Normandie","La Réunion","La Réunion","Midi-Pyrénées","","Rhône-Alpes","Île-de-France","La Réunion","Rhône-Alpes","Île-de-France","Rhône-Alpes","Rhône-Alpes","Rhône-Alpes","Bourgogne","Île-de-France","Bourgogne","Île-de-France","Île-de-France","Île-de-France","Provence-Alpes-Côte d'Azur","Rhône-Alpes","Île-de-France","Alsace","Provence-Alpes-Côte d'Azur","Provence-Alpes-Côte d'Azur","Île-de-France","Provence-Alpes-Côte d'Azur","Provence-Alpes-Côte d'Azur","Picardie","Aquitaine","Rhône-Alpes","Île-de-France","Île-de-France","Île-de-France","Rhône-Alpes","Île-de-France","La Réunion","Bourgogne","Nord-Pas-de-Calais","Nord-Pas-de-Calais","Île-de-France","Lorraine","Île-de-France","Alsace","La Réunion","Nord-Pas-de-Calais","Poitou-Charentes","Rhône-Alpes","Île-de-France","Île-de-France","Centre-Val de Loire","Île-de-France","Île-de-France","Aquitaine","Nord-Pas-de-Calais","Île-de-France","Île-de-France","Île-de-France","La Réunion","Haute-Normandie","Île-de-France","Lorraine","Île-de-France","Île-de-France","Île-de-France","Île-de-France","Rhône-Alpes"]
# Chargement des clients
retailers = ["Aldi","Auchan", "Amazon","Bricomarché","Casino","Carrefour","Carrefour Market","Champion","Colruyt","Cora","E.Leclerc","Écomarché","Franprix","Géant","Hypercacher","Hyper U","Inno","Intermarché","Le Relais des Mousquetaires","Leader Price","LIDL","Marché Plus","Marché U","Monoprix","Netto","Roady","Simply Market","Spar","Super U","Supermarchés G20","Utile","Vêti","Vival"]


# val rnd = new scala.util.Random
def getRndBias(min, max, bias, influence):
    x = min + random.randint(0, (max - min) )
    mix = random.random() * influence
    return math.ceil(x * (1 - mix) + bias * mix)


def getRndMode(min, max, mode, influence):
    if random.random() > influence:
        return min + random.randint(0, (max - min) )
    else:
        return mode


def generateProduct(selectRetailer= 0,selectCity= 0,selectId= 0,selectWeight= 0,retailerInfluence= 0.0, cityInfluence= 0.0, idInfluence= 0.0, weightInfluence= 0.0):
    r = getRndMode(0,32,selectRetailer,retailerInfluence)
    v = getRndMode(0,263,selectCity,cityInfluence)
    id = getRndBias(1,10000,selectId,idInfluence)
    poids = getRndBias(1,200,selectWeight,weightInfluence)
    return Product(id, poids, retailers[r], villes[v], regions[v])


def show_rdd(rdd):
    for x in rdd.collect():
        print(x)

    print(f"Count: {rdd.count()}")


def main():
    # Génération des produits
    # spark = SparkSession.newSession()
    # spark = SparkSession \
    #     .builder \
    #     .appName("Reactive Architecture Learning Demo") \
    #     .getOrCreate()
    # sc = spark.sparkContext

    start_time = time.time()
    print(f"Starting at: {start_time}...")

    print("Checking dependency on Product...")
    pp = Product(1, 2., "CheckRetailer", "CC", "RR")
    print(pp)

    def getSqlContextInstance(sparkContext):
        if ('sqlContextSingletonInstance' not in globals()):
            globals()['sqlContextSingletonInstance'] = SQLContext(sparkContext)
        return globals()['sqlContextSingletonInstance']

    sc = SparkContext(appName="Reactive Architecture Learning Demo")
    spark = getSqlContextInstance(sc)

    # id_rdd = sc.parallelize(range(0, 1000000), 6)
    id_rdd = sc.parallelize(range(0, 100), 6)

    def gen_prod(x):
        return generateProduct()

    # productsRDD = id_rdd.map(lambda i: generateProduct())
    productsRDD = id_rdd.map(gen_prod)
    show_rdd(productsRDD)

    # productsDF = spark.createDataFrame(productsRDD)
    # productsDF.createOrReplaceTempView("products")

    # Pour chaque produit, associe le revendeur au le poid
    # Force 6 partitions pour la démo
    # Regroupe la liste des poids par revendeur
    # Calcul de la somme des poids pour chaque revendeur
    def get_retailer_and_weight(p):
        return p.retailer, p.weight

    def get_retailer_and_weight_sum(p):
        return p[0], sum(p[1])

    # weight_per_retailer = productsRDD.map(lambda product: (product.retailer, product.weight)) \
    #     .coalesce(numPartitions = 6, shuffle=True) \
    #     .groupByKey() \
    #     .map(lambda productByRetailer: (productByRetailer[0], sum(productByRetailer[1]))) \
    #     .collect()

    weight_per_retailer = productsRDD.map(get_retailer_and_weight) \
        .coalesce(numPartitions = 6, shuffle=True) \
        .groupByKey() \
        .map(get_retailer_and_weight_sum) \
        .collect()

    print(weight_per_retailer)
    print(f'Stopping at: {time.time()}')

    sc.stop()


if __name__ == '__main__':
    main()
