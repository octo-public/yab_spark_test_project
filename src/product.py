# Définition des produits
class Product(object):
    def __init__(self, id, weight, retailer, city, region):
        self.region = region
        self.city = city
        self.weight = weight
        self.id = id
        self.retailer = retailer

    def __repr__(self):
        return f"{self.retailer}, {self.id}, {self.weight}, {self.city}, {self.region}]"
