export PROJECT=sandbox-octo-2;export HOSTNAME=test-cython-m;export ZONE=us-central1-b
export PORT=1080
gcloud compute ssh ${HOSTNAME} \
    --project=${PROJECT} --zone=${ZONE}  -- \
    -D ${PORT} -N
/usr/bin/google-chrome \
    --proxy-server="socks5://localhost:${PORT}" \
    --user-data-dir=/tmp/${HOSTNAME}
